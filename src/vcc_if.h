/*
 * NB:  This file is machine generated, DO NOT EDIT!
 *
 * Edit vmod.vcc and run vmod.py instead
 */

struct sess;
struct VCL_conf;
struct vmod_priv;

void vmod_append(struct sess *, enum gethdr_e, const char *, const char *, ...);
void vmod_remove(struct sess *, struct vmod_priv *, enum gethdr_e, const char *, const char *);
const char * vmod_get(struct sess *, struct vmod_priv *, enum gethdr_e, const char *, const char *);
void vmod_copy(struct sess *, enum gethdr_e, const char *, enum gethdr_e, const char *);
const char * vmod_version(struct sess *);
int init_function(struct vmod_priv *, const struct VCL_conf *);
extern const void * const Vmod_Id;
