/*
 * NB:  This file is machine generated, DO NOT EDIT!
 *
 * Edit vmod.vcc and run vmod.py instead
 */

#include "vrt.h"
#include "vcc_if.h"
#include "vmod_abi.h"


typedef void td_header_append(struct sess *, enum gethdr_e, const char *, const char *, ...);
typedef void td_header_remove(struct sess *, struct vmod_priv *, enum gethdr_e, const char *, const char *);
typedef const char * td_header_get(struct sess *, struct vmod_priv *, enum gethdr_e, const char *, const char *);
typedef void td_header_copy(struct sess *, enum gethdr_e, const char *, enum gethdr_e, const char *);
typedef const char * td_header_version(struct sess *);

const char Vmod_Name[] = "header";
const struct Vmod_Func_header {
	td_header_append	*append;
	td_header_remove	*remove;
	td_header_get	*get;
	td_header_copy	*copy;
	td_header_version	*version;
	vmod_init_f	*_init;
} Vmod_Func = {
	vmod_append,
	vmod_remove,
	vmod_get,
	vmod_copy,
	vmod_version,
	init_function,
};

const int Vmod_Len = sizeof(Vmod_Func);

const char Vmod_Proto[] =
	"typedef void td_header_append(struct sess *, enum gethdr_e, const char *, const char *, ...);\n"
	"typedef void td_header_remove(struct sess *, struct vmod_priv *, enum gethdr_e, const char *, const char *);\n"
	"typedef const char * td_header_get(struct sess *, struct vmod_priv *, enum gethdr_e, const char *, const char *);\n"
	"typedef void td_header_copy(struct sess *, enum gethdr_e, const char *, enum gethdr_e, const char *);\n"
	"typedef const char * td_header_version(struct sess *);\n"
	"\n"
	"struct Vmod_Func_header {\n"
	"	td_header_append	*append;\n"
	"	td_header_remove	*remove;\n"
	"	td_header_get	*get;\n"
	"	td_header_copy	*copy;\n"
	"	td_header_version	*version;\n"
	"	vmod_init_f	*_init;\n"
	"} Vmod_Func_header;\n"
	;

const char * const Vmod_Spec[] = {
	"header.append\0Vmod_Func_header.append\0VOID\0HEADER\0STRING_LIST\0",
	"header.remove\0Vmod_Func_header.remove\0VOID\0PRIV_CALL\0HEADER\0STRING\0",
	"header.get\0Vmod_Func_header.get\0STRING\0PRIV_CALL\0HEADER\0STRING\0",
	"header.copy\0Vmod_Func_header.copy\0VOID\0HEADER\0HEADER\0",
	"header.version\0Vmod_Func_header.version\0STRING\0",
	"INIT\0Vmod_Func_header._init",
	0
};
const char Vmod_Varnish_ABI[] = VMOD_ABI_Version;
const void * const Vmod_Id = &Vmod_Id;

